---
title: About me
subtitle: Why you'd want to hang out with me
comments: false
---

This is my personal site. Currently I'm in the process of porting over my old Jekyll site to Hugo... I mean, lots of work but Jekyll is driving me absolutely nuts ;)